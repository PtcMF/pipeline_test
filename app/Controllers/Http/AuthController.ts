import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class AuthController {
    public async login({ request, auth }: HttpContextContract) {
        const email = request.input('email')
        const password = request.input('password')
        console.log('loginloginloginloginloginloginlogin')
        console.log(email)
        console.log(password)
        const token = await auth.use('api').attempt(email, password)

        let res = { authToken: token.toJSON() }
        if (token.user.profile === 'supplier') {
            console.log('token ' + token.user.id)
            const sup = await User.query()
                .preload('suppliers')
                .where('id', token.user.id)
                .firstOrFail()
            console.log(sup)
            res['user'] = sup
        } else {
            res['user'] = token.user
        }
        console.log(res)
        return res
    }
}
