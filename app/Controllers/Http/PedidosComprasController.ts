import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import PedidosCompra from 'App/Models/PedidosCompra'
import PedidosProduto from 'App/Models/PedidosProduto'
import axios from 'axios'
import blingsController from './BlingsController'
import Database from '@ioc:Adonis/Lucid/Database'
import NovosProdutos from 'App/Models/NovosProdutos'

export default class PedidosComprasController {
    public async getAll() {
        const pedidos = await PedidosCompra.all()
        let retPedidos = [] as any
        for (let index = 0; index < pedidos.length; index++) {
            const element = pedidos[index]
            let url =
                'https://bling.com.br/Api/v2/produto/' +
                element.produtoCodigo +
                '/json/?apikey=3b24c69a6e74f2b73d9625e0ee87bb72812737d1a9ad8a1a920f65c7a76e764fda46325f'
            const response = await axios.get(url)
            let produto = response.data.retorno.produtos[0].produto
            produto.produtoCodigo = element.produtoCodigo
            retPedidos.push(produto)
        }
        return retPedidos
    }
    public async lastPurchaseByProduct(params) {
        console.log('lastPurchaseByProduct XXXXXXXXXXXXXXXXXXXXX')
        let { codigoProduto } = params.params
        const itemPedido = await PedidosProduto.query()
            .innerJoin(
                'pedidos_compras',
                'pedidos_produtos.pedidos_compra_id',
                'pedidos_compras.id'
            )
            .preload('pedidosCompra')
            .where('codigo_produto', codigoProduto)
            .where('pedidos_compras.status', 'confirmed')
            .orderBy('pedidos_compras.created_at', 'desc')
            .first()


        return itemPedido
    }
    public async edit(params) {
        console.log('edit XXXXXXXXXXXXXXXXXXXXX')
        let { idpurchase } = params.params
        const pedidos = PedidosCompra.query().preload('products').preload('notas').where('id', idpurchase)
        let pedido = (await pedidos)[0]
        let result = pedido.serialize()

        let blingctrl = new blingsController()
        if (result.products) {
            let totalPedido = parseFloat(result.valor_frete)
            for (let i = 0; i < result.products.length; i++) {
                let element = result.products[i]
                totalPedido += parseFloat(element.quantidade) * parseFloat(element.valor_unitario)
                let product = await blingctrl.getProdutoByCode(element.codigo_produto)
                if(!product)
                {
                    //VERIFICAR EM NOVOS PRODUTOS
                    let nProd = await PedidosProduto.query().preload('newproduct').where('id_prod_ped', element.id_prod_ped)
                    let reg = (await nProd)[0]
                    let prod_ser = reg.serialize()
                    if(prod_ser.newproduct.length > 0)
                    {
                        element.descricao = prod_ser.newproduct[0].nome_produto
                    }
                }else{
                    element.descricao = product.descricao
                    element.estoqueAtual = product.estoqueAtual
                }
                let lastBuy = await this.lastPurchaseByProduct({
                    params: {
                        codigoProduto: element.codigo_produto,
                    },
                })
                element.lastBuy = lastBuy

            }
            result.total_pedido = totalPedido.toFixed(2)
        }
        if (result.notas) {
            for (let i = 0; i < result.notas.length; i++)
            {
                let refNota = result.notas[i]
                let nota = await blingctrl.getXmlNotaByNumero({params:{serie:refNota.serie, numero:refNota.numero}})
                if(nota)
                {
                    refNota.chave = nota[0].notafiscal.chaveAcesso
                    refNota.det = nota[0].det
                }else{
                    refNota.chave = 'NOTA NÃO ENCONTRADA'
                    refNota.det = []
                }
            }
        }
        return result
    }
    public async getByFornecedor(params) {
        console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
        let { idFabricante, codFornecedor } = params.params

        let blingctrl = new blingsController()
        let produtosFornecedor = await blingctrl.listProdutosByFornecedor(params)

        let ret = produtosFornecedor.filter((element) => {
            return element.produto.idFabricante === idFabricante
        })
        let prodcodes = [] as any

        for (let index = 0; index < ret.length; index++) {
            let produto = ret[index].produto

            let pedidos = await this.getPurchasesByProductCode(produto.codigo);
            if (pedidos.length > 0) {
                produto['pedidos'] = pedidos
            }
            prodcodes.push(produto.codigo)

        }
        //BUSCA PRODUTOS NOVOS QUE NÃO ESTÃO BLING
        if(prodcodes.length > 0)
        {
            let params = JSON.stringify(prodcodes)
            params = params.replace('[','(').replace(']',')');
            let sql = 'select * from pedidos_produtos'
            sql += ' inner join pedidos_compras on  pedidos_compras.id = pedidos_produtos.pedidos_compra_id'
            sql += ' inner join novos_produtos on  novos_produtos.pedidos_produto_id_prod_ped = pedidos_produtos.id'
            sql += ' where pedidos_produtos.codigo_produto not in ' + params
            sql += " and pedidos_compras.codigo_fornecedor = '" + codFornecedor + "'"

            sql = sql.split('"').join("'");

            let ped_prod = await Database.rawQuery(sql)
            for (let i = 0; i < ped_prod.rows.length; i++)
            {
                let produto = {descricao:ped_prod.rows[i].nome_produto,
                    codigo : ped_prod.rows[i].codigo_produto,
                    estoqueAtual : 0
                }
                let pedidos = await this.getPurchasesByProductCode(produto.codigo);
                if (pedidos.length > 0) {
                    produto['pedidos'] = pedidos
                }
                let el = {produto:produto}
                ret.push(el)
            }
        }

        return ret
    }
    public async getPurchasesByProductCode (codigo_produto)
    {
        const pedidos = await PedidosProduto.query()
                .preload('pedidosCompra', (query) => {
                    query.orderBy('previsaoEntrega', 'asc')
                })
                .where('codigoProduto', codigo_produto)
        return pedidos
    }
    public async store({ request }: HttpContextContract) {
        try {
            const data = request.only([
                'codigo_fornecedor',
                'valor_frete',
                'observacao',
                'condicao_pagamento',
                'previsao_entrega',
                'data_pedido',
                'status',
                'codigo_pedido',
            ])
            console.log('storestorestorestorestorestorestore')
            console.log(data)
            const pedido = await PedidosCompra.create(data)
            const dataProducts = request.all()
            let newProducts = [] as any
            dataProducts.products.forEach((element) => {
                delete element['descricao']
                delete element['total_item']
                if(element.newProduct)
                {
                    newProducts.push({
                        'codigo_produto' : element.newProduct.codigo_produto,
                        'nome_produto' : element.newProduct.nome_produto,
                        'codigo_fornecedor' : element.newProduct.codigo_fornecedor,
                    })
                }
                delete element['newProduct']
            })

            const produtos = await pedido.related('products').createMany(dataProducts.products)
            //CASO SEJA NOVO PRODUTO, CRIA CADASTRO TEMPORARO DELE
            if(newProducts.length > 0)
            {
                for (let a = 0; a < newProducts.length; a++)
                {
                    let newProd = newProducts[a];
                    for (let i = 0; i < produtos.length; i++)
                    {
                        if(produtos[i].codigoProduto == newProd.codigo_produto)
                        {
                            delete newProd['codigo_produto']
                            produtos[i].related('newproduct').create(newProd)
                        }
                    }
                }
            }
            return { sucesso: pedido }
        } catch (error) {
            console.log(error)
            return { error: error }
        }
    }
    public async update({ request, params }: HttpContextContract) {
        let result = await Database.transaction(async (trx) => {
            try {
                console.log('77777777777777 UPDATE 7777777777777777777777')
                // console.log(params)
                const id = params.idpurchase
                const data = request.only([
                    'valor_frete',
                    'data_pedido',
                    'codigo_pedido',
                    'observacao',
                    'condicao_pagamento',
                    'previsao_entrega',
                    'status',
                ])
                console.log('UPDATEUPDATEUPDATEUPDATEUPDATE')
                console.log(data)
                const pedido = await PedidosCompra.findByOrFail('id', id)
                pedido.useTransaction(trx)

                const dataProducts = request.all()
                console.log('1')
                for (let i = 0; i < dataProducts.products.length; i++) {
                    let el = dataProducts.products[i]
                    let searchPayload = { id_prod_ped: el.id }
                    console.log(searchPayload)
                    let persistancePayload = {
                        quantidade: el.quantidade,
                        valor_unitario: el.valor_unitario,
                        codigo_produto: el.codigo_produto,

                    }
                    if(searchPayload.id_prod_ped)
                    {
                        await pedido
                            .related('products')
                            .updateOrCreate(searchPayload, persistancePayload)
                    }else{
                        let prod = await pedido.related('products').create(persistancePayload)
                        console.log('-----------------------------------------------')
                        console.log(el)
                        //caso seja um produto novo, fora do bling
                        if(el.newProduct)
                        {
                            let novoProduto = {
                                nomeProduto : el.newProduct.nome_produto,
                                codigoFornecedor : el.newProduct.codigo_fornecedor,

                            }
                            await prod.related('newproduct').create(novoProduto)

                        }
                    }
                }
                //SALVA REFERENCIA DAS NOTAS
                console.log('SALVA REFERENCIA DAS NOTAS')
                for (let i = 0; i < dataProducts.notas.length; i++) {
                    let el = dataProducts.notas[i]
                    let searchPayload = { id_nota_ped: el.id_nota_ped }
                    console.log(searchPayload)
                    let persistancePayload = {
                        numero: el.numero,
                        serie: el.serie,

                    }
                    console.log(persistancePayload)
                    await pedido
                        .related('notas')
                        .updateOrCreate(searchPayload, persistancePayload)

                }
                console.log('TO UPDATE MANY PRODUCTS PEDIDO')
                pedido.merge(data)
                pedido.save()
                const result = await PedidosCompra.findByOrFail('id', id)
                return result
            } catch (error) {
                console.log(error)
                await trx.rollback()
                return error
            }
        })
        return { sucesso: result }
    }
    public async storeNota({ request }: HttpContextContract) {
        try {
            const data = request.only(['products'])
            data.products.forEach((element) => {
                delete element['descricao']
                delete element['total_item']
            })
            // const pedido = await PedidosProduto.createMany(data.products)
            return { sucesso: 'teste' }
        } catch (error) {
            console.log(error)
        }
        return { sucesso: 'errouuu' }
    }
    public async storeProducts({ request }: HttpContextContract) {
        try {
            const data = request.only(['products'])
            data.products.forEach((element) => {
                delete element['descricao']
                delete element['total_item']
            })
            const pedido = await PedidosProduto.createMany(data.products)
            return { sucesso: pedido }
        } catch (error) {
            console.log(error)
        }
        return { sucesso: 'errouuu' }
    }
    public async removeProduct({ request, params }: HttpContextContract) {
        try {
            const idprodped = params.idprodped
            const prd = await PedidosProduto.find(idprodped)
            if(prd)
            {
                await prd.delete()
            }
            return { sucesso: 'temp' }
        } catch (error) {
            console.log(error)
        }
        return { sucesso: 'errouuu' }
    }
}
