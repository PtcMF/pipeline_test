import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import axios from 'axios'

const apikey = '3b24c69a6e74f2b73d9625e0ee87bb72812737d1a9ad8a1a920f65c7a76e764fda46325f'

var xmlparser = require('xml2json');
var arrProdutos = [] as any
var cacheFornecedores = [] as any

export default class BlingsController {
    public async listProdutos({ params }) {
        console.log('listProdutos')
        console.log(params)
        let {filter } = params
        try {
            let response = await this.listAllProdutos({params:params})
            if(filter)
            {
                let resFiltered = response.filter((el) =>
                {
                    let desc = el['produto']['descricao']
                    let cod = el['produto']['codigo']
                    console.log(desc)
                    console.log(cod)
                    console.log(desc.toUpperCase())
                    return desc.toUpperCase().indexOf(filter.toUpperCase()) >= 0 || cod.indexOf(filter) >= 0
                })
                response = resFiltered
            }
            console.log(response.length)
            return response
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async listProdutosFornecedor({ request }: HttpContextContract) {
        console.log('listProdutosFornecedor')
        console.log(request)
        try {
            const response = await axios.get(
                `https://bling.com.br/Api/v2/produtosfornecedores/json/?apikey=${apikey}`
            )
            console.log(response)
            return response.data
        } catch (error) {
            console.error(error)
            return error
        }
    }

    public async listFornecedor(params) {
        console.log('listProdutosFornecedor')
        // console.log(params)
        let { codigoFornecedor } = params.params
        let { forcereload } = params.params
        console.log(codigoFornecedor)
        console.log(forcereload)
        try {
            if (cacheFornecedores.length > 0 && forcereload !== 'true') {
                if (codigoFornecedor) {
                    let xxx = cacheFornecedores.filter((element) => {
                        const found = element.contato.tiposContato.find((el) => {
                            return el.tipoContato.descricao === 'Fornecedor'
                        })
                        return found
                    })
                    console.log(xxx)
                    return xxx
                }
                return cacheFornecedores
            }
            cacheFornecedores = [] as any
            let page = 1
            let next = true
            do {
                next = false
                console.log(page + ' responsecontatos.data XXXXXXXXXXXXXXXXXXXXXXXXXX ' + page)
                let url = `https://bling.com.br/Api/v2/contatos/page=${page}/json/?apikey=${apikey}&estoque=S`
                console.log(url)
                const response = await axios.get(url)
                if (response.data.retorno.contatos) {
                    for (let i = 0; i < response.data.retorno.contatos.length; i++) {
                        let element = response.data.retorno.contatos[i]
                        if (codigoFornecedor && element.contato.codigo !== codigoFornecedor) {
                            continue
                        }
                        if (!element.contato.tiposContato) {
                            continue
                        }
                        const found = element.contato.tiposContato.find((el) => {
                            return el.tipoContato.descricao === 'Fornecedor'
                        })
                        if (found) {
                            cacheFornecedores.push(element)
                        }
                    }
                    page++
                    next = true
                }
            } while (next === true)
            cacheFornecedores.sort(function (a, b) {
                if (a.name > b.name) {
                    return 1
                }
                if (a.name < b.name) {
                    return -1
                }
                // a must be equal to b
                return 0
            })
            return cacheFornecedores
        } catch (error) {
            console.error(error)
            return error
        }
    }

    public async listAllProdutos({ params }) {
        console.log('listAllProdutos')
        let { forcereload } = params
        try {
            if (arrProdutos.length > 0 && forcereload !== 'true') {
                //QUEBRA REFERENCIA PARA DADOS ORIGINAIS NÃO SEREM ALTERADOS
                return JSON.parse(JSON.stringify(arrProdutos))
            }
            arrProdutos = [] as any
            let page = 1
            let next = true
            do {
                next = false
                let url = `https://bling.com.br/Api/v2/produtos/page=${page}/json/?apikey=${apikey}&estoque=S`
                const response = await axios.get(url)
                console.log(
                    page + ' response.data XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ' + arrProdutos.length
                )
                // console.log(arrProdutos)
                if (response.data.retorno.produtos) {
                    arrProdutos = arrProdutos.concat(response.data.retorno.produtos)
                    page++
                    next = true
                }
            } while (next === true)
            return arrProdutos
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async listProdutosByFornecedor({ params }) {
        console.log('listProdutosByFornecedor')
        let { idFabricante } = params
        idFabricante = idFabricante.replaceAll('%20', ' ')
        try {
            let retProdutos = [] as any
            let allProdutos = await this.listAllProdutos({ params })
            if (allProdutos) {
                for (let i = 0; i < allProdutos.length; i++) {
                    let element = allProdutos[i]
                    if (element.produto.idFabricante === idFabricante) {
                        retProdutos.push(element)
                    }
                }
            }

            return arrProdutos
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async getProdutoByCode(code) {
        console.log('getProdutoByCode ' + code)
        try {
            let url = `https://bling.com.br/Api/v2/produto/${code}/json/?apikey=${apikey}&estoque=S`
            const response = await axios.get(url)
            console.log(response.data)
            return response.data.retorno.produtos ? response.data.retorno.produtos[0].produto : null
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async teste() {
        try {
            const response = await axios.get(
                `https://bling.com.br/Api/v2/produtos/json/?apikey=${apikey}&estoque=S`
            )
            console.log(response)
            return response.data
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async listNotas(params) {
        console.log('listNotas')
        // console.log(params)
        let { tipo } = params.params
        let { situacao } = params.params
        console.log(tipo)
        console.log(situacao)
        var ini = new Date();
        var fim = new Date();
        ini.setDate(fim.getDate()-15);
        let format = (d) =>
        {
            let newDate = d.toISOString()
            newDate = newDate.replace(/\D/g, "");
            newDate = newDate.replace(/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/, "$3/$2/$1 $4:$5:$6");
            newDate = newDate.substring(0,10)
            return newDate
        }
        let iniS = format(ini) + ' 00:00:01';
        let fimS = format(fim) + ' 23:59:59';
        let filters = '';
        if(tipo || situacao)
        {
            filters = `filters=`
            filters += tipo ? `tipo[${tipo}];` : ''
            filters += situacao ? `situacao[${situacao}]`: ''

        }
        filters = `filters=tipo[${tipo}];situacao[${situacao}];dataEmissao[${iniS} TO ${fimS}]`
        // filters = `filters=tipo[${tipo}];situacao[${situacao}];dataEmissao[30/05/2021 00:01:00 TO 31/05/2021 23:59:59]`
        try {
                let url = `https://bling.com.br/Api/v2/notasfiscais/json/?apikey=${apikey}&${filters}`
                console.log(url)
                const response = await axios.get(url)
                console.log(response.data)
                console.log(response.data.retorno.erros)
                if(response.data.retorno.erros)
                {
                    return response.data.retorno.erros[0].erro
                }
            return response.data.retorno.notasfiscais
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async getXmlNotaByNumero(params) {
        try {
            let nota = await this.getNotaByNumero(params)
            console.log('getXmlNotaByNumero xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
            console.log(nota)
            if(nota.erro)
            {
                return null
            }
            let url = nota[0].notafiscal.xml
            const response = await axios.get(url)
            // console.log(response.data)

            var json = xmlparser.toJson(response.data);
            json = JSON.parse(json)
            console.log(json.nfeProc.NFe.infNFe);
            console.log(json.nfeProc.NFe.infNFe.det);
            nota[0].det = json.nfeProc.NFe.infNFe.det;
            return nota
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async getNotaByNumero(params) {
        console.log('getNotaByNumero xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        let { serie } = params.params
        let { numero } = params.params
        try {
                let url = `https://bling.com.br/Api/v2/notafiscal/${numero}/${serie}/json/?apikey=${apikey}`
                console.log(url)
                const response = await axios.get(url)
                if(response.data.retorno.erros)
                {
                    return response.data.retorno.erros[0]
                }
            return response.data.retorno.notasfiscais
        } catch (error) {
            console.error(error)
            return error
        }
    }
    public async generateDanfe(params) {
        console.log('generateDanfe')
        // console.log(params)
        // let { url } = params.url
        // console.log(url)

        try {
                // let url = `https://bling.com.br/Api/v2/notasfiscais/json/?apikey=3b24c69a6e74f2b73d9625e0ee87bb72812737d1a9ad8a1a920f65c7a76e764fda46325f&${filters}`
                let fs1 = require('fs')
                console.log('1')
                const http = require("http");
                console.log('2')
                const file = fs1.createWriteStream("nota.xml");
                console.log(file)
                console.log('3')
                console.log(http)
                await http.get("https://bling.com.br/relatorios/nfe.xml.php?s&chaveAcesso=41210502551354000150550010000064961243592102",
                response => {
                    console.log('RESPONSE')
                    response.pipe(file);
                });
                console.log('4')
                console.log(file)
                console.log(fs1.readFileSync(file).toString())
                var webdanfe = require('webdanfe'),
                fs = require('fs'),
                xml = fs.readFileSync("https://bling.com.br/relatorios/nfe.xml.php?s&chaveAcesso=41210502551354000150550010000064961243592102").toString();

                console.log(xml)
                webdanfe.gerarDanfe(xml, function(err, pdf) {
                    if(err) {
                        throw err;
                    }

                    fs.writeFileSync('danfe.pdf', pdf, {
                        encoding: 'binary'
                    });
                });
            return xml
        } catch (error) {
            console.error(error)
            return error
        }
    }
}
