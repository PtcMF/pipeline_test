import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class VendasController {
    public async genTicket({ request }: HttpContextContract) {
        console.log('genTicket')
        try {
            const data = request.all()

            var fs = require('fs')
            //encontra caminho absoluto dos arquivos
            const path = require('path')
            let dir = path.resolve('resources', 'ticket.html')
            let dir1 = path.resolve('public', 'temp', "example.pdf")

            let html = fs.readFileSync(dir, 'utf8')

            //aplica valores html
            html = await this.replaceTicketValues(data, html)

            //transforma html em pdf
            var html_to_pdf = require('html-pdf-node');
            let options = { format: 'A4' };
            let file = { content: html, name: "example.pdf" };
            await html_to_pdf.generatePdf(file, options).then( async (pdfBuffer) => {
                await fs.writeFile(dir1, pdfBuffer,  "binary",function(err) {
                    console.log(err)
                 });
              });
            return { success: 'sucesso', url :   '/temp/example.pdf'}
        } catch (error) {
            console.log(error)
        }
        return { sucesso: 'errouuu' }
    }
    private async replaceTicketValues(params, html)
    {
        console.log('replaceTicketValues')
        for (var key in params) {
            let htmlField = '[#'+key+'#]'
            html = html.replace(htmlField, params[key])
        }
        return html
    }


}
