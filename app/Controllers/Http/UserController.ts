import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import User from 'App/Models/User'
import UserSupplier from 'App/Models/UserSupplier'

export default class UserController {
    public async create({ request }: HttpContextContract) {
        console.log('create YYYYYYYYYYYYYYYYYYYYYYYYYYYY')
        const data = request.only(['email', 'password', 'profile', 'name'])
        const supplier = request.only(['supplier_code'])
        console.log(data)
        console.log(supplier)

        const user = User.create(data)
        if (supplier.supplier_code) {
            let reg = { userId: (await user).id, supplierCode: supplier.supplier_code }
            console.log(reg)
            const x = UserSupplier.create(reg)
            console.log('xxxxxxxxx')
            console.log(x)
        }
        return user
    }
}
