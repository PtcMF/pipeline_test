import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import PedidosCompra from './PedidosCompra'
import NovosProdutos from './NovosProdutos'

export default class PedidosProduto extends BaseModel {
    @column({ isPrimary: true, columnName: 'id' })
    public id_prod_ped: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public codigoProduto: String

    @column()
    public quantidade: Number

    @column()
    public valorUnitario: Number

    @column()
    public desconto: Number

    @column()
    public pedidosCompraId: number

    @belongsTo(() => PedidosCompra)
    public pedidosCompra: BelongsTo<typeof PedidosCompra>

    @hasMany(() => NovosProdutos)
    public newproduct: HasMany<typeof NovosProdutos>
}
