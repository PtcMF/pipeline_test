import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import PedidosProduto from './PedidosProduto'
import NotasPedidos from './NotasPedidos'

export default class PedidosCompra extends BaseModel {
    @column({ isPrimary: true })
    public id: number

    @column()
    public codigoPedido: String

    @column()
    public codigoFornecedor: String

    @column()
    public valorFrete: Number

    @column()
    public totalPedido: Number

    @column.date({ autoCreate: false })
    public previsaoEntrega: DateTime

    @column.date({ autoCreate: false })
    public dataPedido: DateTime

    @column()
    public condicaoPagamento: String

    @column()
    public observacao: String

    @column()
    public status: String

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @hasMany(() => PedidosProduto)
    public products: HasMany<typeof PedidosProduto>

    @hasMany(() => NotasPedidos)
    public notas: HasMany<typeof NotasPedidos>

}
