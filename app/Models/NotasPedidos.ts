import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import PedidosCompra from './PedidosCompra'

export default class NotasPedidos extends BaseModel {
    @column({ isPrimary: true, columnName: 'id' })
    public id_nota_ped: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public chave: String

    @column()
    public serie: String

    @column()
    public numero: String

    @column()
    public pedidosCompraId: number

    @belongsTo(() => PedidosCompra)
    public pedidosCompra: BelongsTo<typeof PedidosCompra>
}
