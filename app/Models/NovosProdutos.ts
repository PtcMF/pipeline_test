import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, BelongsTo } from '@ioc:Adonis/Lucid/Orm'
import PedidosProduto from './PedidosProduto'

export default class NovosProdutos extends BaseModel {
    @column({ isPrimary: true, columnName: 'id' })
    public id: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public codigoProduto: String

    @column()
    public nomeProduto: String

    @column()
    public codigoFornecedor: String

    @column()
    public pedidosProdutoIdProdPed: number

    @belongsTo(() => PedidosProduto)
    public product: BelongsTo<typeof PedidosProduto>
}
