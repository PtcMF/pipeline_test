import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PedidosCompras extends BaseSchema {
  protected tableName = 'pedidos_compras'

  public async up () {
    this.schema.table(this.tableName, (table) => {
        table.dateTime('data_pedido')
    })
  }

  public async down () {
    this.schema.table(this.tableName, (table) => {
        table.dropColumn('data_pedido')
    })
  }
}
