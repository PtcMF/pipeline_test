import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PedidosProdutos extends BaseSchema {
    protected tableName = 'pedidos_produtos'

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id')
            table.timestamps(true)
            table.string('codigo_produto')
            table.integer('quantidade')
            table.decimal('valor_unitario')
            table.decimal('desconto')
            table.integer('pedidos_compra_id')
        })
    }

    public async down() {
        this.schema.dropTable(this.tableName)
    }
}
