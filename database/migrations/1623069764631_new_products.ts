import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class NovosProdutos extends BaseSchema {
  protected tableName = 'novos_produtos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
        table.increments('id')
        table.timestamps(true)
        table.string('nome_produto')
        table.integer('codigo_fornecedor')
        table.integer('pedidos_produto_id_prod_ped')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
