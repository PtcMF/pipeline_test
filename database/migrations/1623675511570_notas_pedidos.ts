import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class NotasPedidos extends BaseSchema {
  protected tableName = 'notas_pedidos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true)
      table.string('chave')
      table.string('serie')
      table.string('numero')
      table.integer('pedidos_compra_id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
