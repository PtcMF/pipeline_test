import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserSuppliers extends BaseSchema {
    protected tableName = 'user_suppliers'

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id')
            table.timestamps(true)
            table.integer('user_id')
            table.string('supplier_code')
        })
    }

    public async down() {
        this.schema.dropTable(this.tableName)
    }
}
