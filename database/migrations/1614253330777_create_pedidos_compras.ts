import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreatePedidosCompra extends BaseSchema {
    protected tableName = 'pedidos_compras'

    public async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id')
            table.timestamps(true)
            table.string('codigo_pedido')
            table.string('codigo_fornecedor')
            table.decimal('valor_frete')
            table.decimal('total_pedido')
            table.date('previsao_entrega')
            table.string('condicao_pagamento')
            table.string('observacao')
            table.string('status')
        })
    }

    public async down() {
        this.schema.dropTable(this.tableName)
    }
}
