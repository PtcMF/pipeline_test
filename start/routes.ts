/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => ({ hello: 'world' }))
Route.get('/bling/produtos', 'BlingsController.listProdutos')
Route.get('/bling/produtos/:filter', 'BlingsController.listProdutos')
Route.get('/bling1', 'BlingsController.listProdutosFornecedor')
Route.get('/bling2', 'BlingsController.teste')
Route.get('/bling/fornecedores', 'BlingsController.listFornecedor')
Route.get('/bling/fornecedores/:codigoFornecedor/:forcereload', 'BlingsController.listFornecedor')
Route.get('/bling/notas', 'BlingsController.listNotas')
Route.get('/bling/nota/:serie/:numero', 'BlingsController.getNotaByNumero')
Route.get('/bling/danfe', 'BlingsController.generateDanfe')
// Route.get('/bling/notas/:numero/:serie', 'BlingsController.listNotas')
Route.get('/bling/notas/:tipo/:situacao', 'BlingsController.listNotas')
Route.get('/bling4', 'BlingsController.listProdutosByFornecedor')

Route.post('/notas/genticket/', 'VendasController.genTicket')

Route.get('/pedidos/list/:idFabricante/:codFornecedor', 'PedidosComprasController.getByFornecedor')
Route.get('/pedidos/list/:idFabricante/:codFornecedor/:forcereload', 'PedidosComprasController.getByFornecedor')
Route.get('/pedidos/edit/:idpurchase', 'PedidosComprasController.edit')
Route.post('/pedidos/create', 'PedidosComprasController.store')
Route.post('/pedidos/update/:idpurchase', 'PedidosComprasController.update')
Route.post('/pedidos/storenota', 'PedidosComprasController.storeNota')
Route.get('/pedidos/removeproduct/:idprodped', 'PedidosComprasController.removeProduct')
Route.get('/pedidos/lastp/:codigoProduto', 'PedidosComprasController.lastPurchaseByProduct')


Route.post('/user/auth', 'UserController.login')
Route.post('/login', 'AuthController.login')
Route.post('/user/create', 'UserController.create')
